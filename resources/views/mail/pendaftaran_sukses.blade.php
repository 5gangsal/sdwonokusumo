@if($data->siswa_pindahan_baru == "baru")
    <p>
        Pengumuman kelulusan mendaftar di sd wonokusumo jaya 127. Melalui pesan ini kami sampaikan bahwa siswa bernama {{$data->nama}} telah diterima untuk masuk kelas 1.
        <br/>Pemberitahuan
        <br/>1. Masuk sekolah pada tanggal {{$data->penerimaan}}
        <br/>2. Menggunakan seragam merah putih panjang ( siswa perempuan menggunakan jilbab)
        <br/>3. Membawa 1 buku kotak besar
        <br/>Demikian pemberitahuan dari kami, terimakasih.
    </p>
@else
    <p>
        Pengumuman kelulusan mendaftar di sd wonokusumo jaya 127. Melalui pesan ini kami sampaikan bahwa siswa bernama {{$data->nama}} telah diterima .
        <br/>Pemberitahuan
        <br/>1. Esok hari langsung masuk sekolah
        <br/>2. Menggunakan seragam merah putih panjang ( siswa perempuan menggunakan jilbab)
        <br/>3. Membawa 1 buku tulis
        <br/>Demikian pemberitahuan dari kami, terimakasih.
    </p>
@endif
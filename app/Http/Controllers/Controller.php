<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Mail;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Store a newly created resource in pdf.
     *
     * @throws \Mpdf\MpdfException
     */
    public function makePDF($html, $title_file){
        $pdf = new \Mpdf\Mpdf();
        $pdf->WriteHTML($html);
        $pdf->Output($title_file.'.pdf', \Mpdf\Output\Destination::DOWNLOAD);
    }

    public function sendMail($data, $from, $to, $subject) {
        $mail = Mail::send('mail.pendaftaran_sukses', ["data"=>$data], function ($m) use ($data,$from,$to,$subject)  {
            $m->from($from["email"], $from["name"]);
            $m->to($to["email"], $to["name"])->subject($subject);
        });
        return $mail;
    }
}


